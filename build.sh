apt-get install build-essential -y
apt-get install libpcre3-dev -y

wget http://www.haproxy.org/download/1.8/src/haproxy-1.8.3.tar.gz
tar -xvf haproxy-1.8.3.tar.gz

cd /haproxy-1.8.3

make TARGET=linux2628 USE_LIBCRYPT=1

make install

cp -R /usr/local/sbin/haproxy /usr/sbin

cp -R /haproxy-1.8.3/examples/haproxy.init /etc/init.d/haproxy

sudo chmod +x /etc/init.d/haproxy

mkdir -p /etc/haproxy

vi /etc/haproxy/haproxy.cfg

echo "global
	log /dev/log	local0
	log /dev/log	local1 notice
	chroot /var/lib/haproxy
	stats socket /run/haproxy/admin.sock mode 660 level admin
	stats timeout 30s
	user haproxy
	group haproxy
	daemon

	# Default SSL material locations
	ca-base /etc/ssl/certs
	crt-base /etc/ssl/private

	# Default ciphers to use on SSL-enabled listening sockets.
	# For more information, see ciphers(1SSL). This list is from:
	#  https://hynek.me/articles/hardening-your-web-servers-ssl-ciphers/
	ssl-default-bind-ciphers ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5:!DSS
	ssl-default-bind-options no-sslv3

defaults
	log	global
	mode	http
	option	httplog
	option	dontlognull
        timeout connect 5000
        timeout client  50000
        timeout server  50000
	errorfile 400 /etc/haproxy/errors/400.http
	errorfile 403 /etc/haproxy/errors/403.http
	errorfile 408 /etc/haproxy/errors/408.http
	errorfile 500 /etc/haproxy/errors/500.http
	errorfile 502 /etc/haproxy/errors/502.http
	errorfile 503 /etc/haproxy/errors/503.http
	errorfile 504 /etc/haproxy/errors/504.http
listen myapp 0.0.0.0:80
    mode http
    stats enable
    stats uri /haproxy?stats
    stats realm Strictly\ Private
    stats auth A_Username:YourPassword
    stats auth Another_User:passwd
    balance roundrobin
    option httpclose
    option forwardfor
    server web-node-01 172.17.0.3:80 check
    server web-node-02 172.17.0.4:80 check" >> /etc/haproxy/haproxy.cfg

service haproxy start



